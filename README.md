
********************************************************************************
--------------------------------------------------------------------------------
************************                               *************************
-------------------------  		    LABYMON		     ---------------------------
************************                               *************************
--------------------------------------------------------------------------------
********************************************************************************


Projet de fin d'etude en Programmation Fonctionnelle
--------------------------------------------------------------------------------
Encadr� par M. Pierre BOUDES
R�alis� par Steven YUNG et Marie-Eve PICARD GRAVEL
Cr�dit image : Kenney Vleugels - http://www.kenney.nl/\n

================================================================================
Ce projet concerne la r�alisation d'un jeu en Ocaml graphique.

Nous avons commenc� ce projet en utilisant les fichiers fournis par notre 
enseignant comme base pour nous orienter. 

Ces fichiers �taient les suivants :
- dessiner.ml
- directions.ml
- interprete.ml
- images50.ml
- astro.ml

Le premier permettait de dessiner une image � l'�cran.
Le second permettait de r�f�rencer le type direction.
Le troisi�me permettait d'utiliser le type direction en faisant r�f�rence au 
fichier "directions.ml" vu comme un module. Il permettait aussi de d�finir un 
type instruction et une fonction run pour pouvoir effectuer des actions. 
Le quatri�me permettait de r�f�rencer des images sous forme de matrice d'entiers.
Le dernier permettait d'afficher une grille bleue avec des cases de 50*50 pixels
sur lesquelles figuraient des �toiles. Un personnage �tait aussi affich�, et 
pouvait se d�placer dans toutes les directions � l'aide de l'interpr�te. Ceci 
afin de ramasser der �toiles.

Ces fichiers �taient disponibles via GitHub qu'il a fallu installer sur nos 
ordinateurs personnels afin de les r�cup�rer mais aussi pour nous permettre de 
nous cr�er un dossier de projet qui puisse nous faciliter la t�che lorsque nous
allions travailler simultan�ment sur nos fichiers. En effet, ce proc�d� nous a 
permis de ne pas �craser nos fichiers car il y avait la possibilit� de revenir 
aux pr�c�dentes versions d'un fichier en cas de probl�me. En plus, nous pouvions 
savoir qui avait ajout� quoi et � quel moment cela avait �t� ajout� afin de mieux
situer notre avancement et le partage des t�ches. 

Pour notre projet LabyMon, nuos avions eu l'id�e de cr�er une grille sur laquelle
afficher un fond en pelouse et en sable sur lequel positionner des rochers � la
mani�re d'un labyrinthe. Nous avions alors pens� qu'il pourrait �tre int�ressant
de positionner des objets dans certaines parties du labyrinthe, ainsi que des 
monstres qu'il faudrait combattre pour acc�der aux objets � ramasser. Nous avons 
alors pens� � conserver les �toiles mais aussi ajouter des coeurs pour mettre en
place un syst�me de "vies" ou de "points de vie" � r�cup�rer en ramassant un coeur
et que l'on peut perdre lors d'un combat contre un monstre. Nous avons alors pens� 
qu'un joueur trouverait cela ennuyeux s'il risquait de perdre trop souvent, donc
nous avons imagin� un syst�me de tirage al�atoire lors d'un combat pour que soit 
le joueur perde un coeur, soit le monstre. Sachant que si le monstre perd un coeur 
il meurt puisqu'il n'en poss�de qu'un. Le joueur en poss�de 3 de base et peut en
collecter 4 autres qui se trouvent sur la carte.

Le nombre de d�placements est illimit�. Le jeu se termine lorsque le joueur a 
combattu chaque monstre pour r�cup�rer toutes les �toiles et a trouv� la sortie. 

Pour cr�er ce jeu, nous avons commenc� par dessiner une grille sur papier, de 
dimensions 800*800 pixels avec des cases de 50*50 pixels. Soit, un jeu sur une
grille carr�e de 16 cases en hauteur et en largeur. Nous avons donc entrepris 
de dessiner notre labyrinthe sur papier pour savoir � quelle position placer tel
ou tel objet. 

Ensuite nous avons fait des tests sur les fichiers de base avant de cr�er les
notres.

Puis nous avons cr�� nos fichiers d'images :
- imgDecors.ml
- imgPersos.ml
- imgObjets.ml

Ces trois fichiers contenant les matrices d'entiers repr�sentant les images que 
nous souhaitions utiliser pour notre jeu. Dans le premier fichier il y a donc les 
images relatives au d�cor. Dans le second il y a celles qui concernent les personnages
de type joueur ou de type monstre. Dans le troisi�me il y a les images de objets 
comme une �toile, un rocher ou un coeur.

Ensuite nous avons commenc� � cr�er notre fichier de jeu :
- labymon.ml

Ce fichier reprend une partie du code du fichier astro.ml qui nous avait �t� donn�
par notre enseignant mais nous l'avons modifi� de mani�re � montrer que nous comprenons
bien l'implication de tout ce que nous r�utilisons. Nous avons �galement fait �voluer 
ce fichier. 

Notamment : 
- La taille de la fen�tre n'est plus de 350*350 pixels mais de 800*800 pixels.
- Le terrain n'est plus simplement bleu mais contient un d�cor compos� d'images.
- Les images utilis�es pour le d�cor, les personnages et les objets ne sont plus les 
m�mes car nous en avons ajout� et avons utilis� les fichiers d'images que nous avons 
cr��s dans ce but.
- La fonction d�placer � �t� modifi�e de mani�re � ce que les d�placements du personnage
soient limit�s � la grille, car dans la version de base il pouvait sortie du champ de 
vision. Nous avons �galement interdit au personnage de se d�placer sur une case sur
laquelle il y a un rocher car celui-ci lui bloque le passage. Sinon il ne serait pas 
demeur� � l'int�rieur du labyrinthe.

TODO - Suite

Pour compiler ce jeu, il faut entrer :
- ocamlmktop directions.ml graphics.cma dessiner.ml imgDecors.ml imgPersos.ml imgObjets.ml interprete.ml labymon.ml -o Labymon

L'ordre d'�criture des fichiers lors de la compilation est important car, si l'on tente de compiler avec la commande suivante : 
" ocamlmktop directions.ml dessiner.ml graphics.cma imgDecors.ml imgPersos.ml imgObjets.ml interprete.ml labymon.ml -o Labymon" 
une erreur se produit :

File "labymon.ml", line 1:
Error: Error while linking dessiner.cmo:
Reference to undefined global `Graphics'

------------------------------------------------------
Pour jouer, il faut entrer : 
open Interprete ;;
open Labymon ;;
./Labymon