open Directions;;
module Personnage = ImgPersos;;
module Objets = ImgObjets;;
module Decors = ImgDecors;;

(*** Personnage du Joueur ***)
let perso = Personnage.filleCornue;;

(*** Monstres Vivants ***)
let chauveSouris = Personnage.chauveSouris;;
let serpent = Personnage.serpent;;
let grenouille = Personnage.grenouille;;
let araignee = Personnage.araignee;;
let fantome = Personnage.fantome;;
let abeille = Personnage.abeille;;

(*** Monstres Morts ***)
let chauveSouris_morte = Personnage.chauveSouris_Morte;;
let serpent_mort = Personnage.serpent_Mort;;
let grenouille_morte = Personnage.grenouille_Morte;;
let araignee_morte = Personnage.araignee_Morte;;
let fantome_mort = Personnage.fantome_Mort;;
let abeille_morte = Personnage.abeille_Morte;;

(*** Le module Graphics Ouvre une fen�tre ***)
(** Pour fermer : Graphics.close_graph ();; **)
Graphics.open_graph " 800x800";;
Graphics.set_color (Graphics.rgb 0 40 180);;
Graphics.fill_rect 0 0 800 800;;
Graphics.set_color (Graphics.rgb 0 128 255);;
	  
(*** Grille de Jeu ***)
let f i =
  Graphics.draw_segments [|
      (50*i,0,50*i,800);
      (0,50*i,800,50*i)
     |];
    in
    List.iter f [1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16]
;;
  
Graphics.remember_mode false;;
  
let afficher img (i,j) = Dessiner.dessiner_image img (!j * 50) (750 - !i * 50);;
  
(*** D�clarations Points de Vie ***)
let nb_pointsDeVie_perso = ref 3;;  

(*** Chance de rejouer ***)
let nb_chance = ref 0;;  
  
(*** Pour la sortie ***)
(** let sortie_trouvee = ref 0;; **)
	    
(*** R�f�rencement des cases ***)
(** Pour affichage des objets **)
(* Etoiles *)
let etoiles = [(ref 2, ref 13);
	       (ref 2, ref 15);
	       (ref 4, ref 14);
	       (ref 5, ref 4);
	       (ref 7, ref 2);
	       (ref 12,ref 8);
	       (ref 15,ref 14)]				
;;
	    
(* Coeurs *)				
let coeurs = [(ref 1, ref 8);
	      (ref 4, ref 12);
	      (ref 4, ref 14);
	      (ref 13, ref 4);
	      (ref 13, ref 14)]
;;	
  
(* Rochers [Labyrinthe] *)				
let rochers = [(ref 1, ref 9);
	       (ref 1, ref 10);
	       (ref 1, ref 11);
	       (ref 1, ref 12);
	       (ref 1, ref 13);
	       (ref 1, ref 14);
	       
	       (ref 2, ref 2);
	       (ref 2, ref 3);
	       (ref 2, ref 4);				
	       (ref 2, ref 5);
	       (ref 2, ref 6);
	       (ref 2, ref 7);				
	       (ref 2, ref 8);
	       (ref 2, ref 9);
	       (ref 2, ref 14);
	       
	       (ref 3, ref 2);
	       (ref 3, ref 11);
	       (ref 3, ref 12);				
	       (ref 3, ref 13);
	       (ref 3, ref 14);
	       (ref 3, ref 15);	
	       
	       (ref 4, ref 2);
	       (ref 4, ref 4);
	       (ref 4, ref 5);				
	       (ref 4, ref 7);
	       (ref 4, ref 8);
	       (ref 4, ref 10);				
	       (ref 4, ref 11);
	       (ref 4, ref 13);
	       (ref 4, ref 15);

	       (ref 5, ref 2);
	       (ref 5, ref 5);
	       (ref 5, ref 7);				
	       (ref 5, ref 13);
	       (ref 5, ref 15);
	       
	       (ref 6, ref 1);
	       (ref 6, ref 2);
	       (ref 6, ref 3);				
	       (ref 6, ref 4);
	       (ref 6, ref 5);
	       (ref 6, ref 7);				
	       (ref 6, ref 9);
	       (ref 6, ref 10);
	       (ref 6, ref 11);				
	       (ref 6, ref 12);
	       (ref 6, ref 13);
	       (ref 6, ref 15);

	       (ref 7, ref 1);
	       (ref 7, ref 3);
	       (ref 7, ref 7);				
	       (ref 7, ref 9);
	       (ref 7, ref 13);
	       (ref 7, ref 15);

	       (ref 8, ref 1);
	       (ref 8, ref 3);
	       (ref 8, ref 5);				
	       (ref 8, ref 11);
	       (ref 8, ref 13);
	       (ref 8, ref 15);

	       (ref 9, ref 1);
	       (ref 9, ref 5);
	       (ref 9, ref 6);				
	       (ref 9, ref 7);
	       (ref 9, ref 9);
	       (ref 9, ref 10);				
	       (ref 9, ref 11);
	       (ref 9, ref 13);
	       (ref 9, ref 15);

	       (ref 10, ref 1);
	       (ref 10, ref 3);
	       (ref 10, ref 5);				
	       (ref 10, ref 15);

	       (ref 11, ref 1);
	       (ref 11, ref 3);
	       (ref 11, ref 7);				
	       (ref 11, ref 8);
	       (ref 11, ref 9);
	       (ref 11, ref 11);				
	       (ref 11, ref 13);
	       (ref 11, ref 15);	

	       (ref 12, ref 1);
	       (ref 12, ref 3);
	       (ref 12, ref 5);				
	       (ref 12, ref 7);
	       (ref 12, ref 9);
	       (ref 12, ref 11);				
	       (ref 12, ref 13);
	       (ref 12, ref 14);				
	       (ref 12, ref 15);

	       (ref 13, ref 1);
	       (ref 13, ref 5);
	       (ref 13, ref 11);				
	       (ref 13, ref 15);

	       (ref 14, ref 1);
	       (ref 14, ref 2);
	       (ref 14, ref 3);				
	       (ref 14, ref 4);
	       (ref 14, ref 5);
	       (ref 14, ref 6);				
	       (ref 14, ref 7);
	       (ref 14, ref 8);				
	       (ref 14, ref 9);				
	       (ref 14, ref 10);
	       (ref 14, ref 11);				
	       (ref 14, ref 12);
	       (ref 14, ref 13);
	       (ref 14, ref 15)]
;;

(** Pour affichage du personnage **)				
let perso_i, perso_j = ref 8, ref 8;;

(** Pour affichage des monstres **)
(*let chauveSouris_i, chauveSouris_j = ref 5, ref 3 ;;
let serpent_i, serpent_j = ref 2, ref 12 ;;
let grenouille_i, grenouille_j = ref 8, ref 2 ;;
let fantome_i, fantome_j = ref 11, ref 14 ;;
let araignee_i, araignee_j = ref 13, ref 8;;
let abeille_i, abeille_j = ref 1, ref 15;;*)

(* Pour l'attaque des monstres *)
let monstres = [(ref 5, ref 3);
		(ref 2, ref 12);
		(ref 8, ref 2);
		(ref 11, ref 14);
		(ref 13, ref 8);
		(ref 1, ref 15)]
;;

(** Pour affichage du d�cor **)
(* Coin Haut Gauche *)
let terrainHautGauche = [(ref 0, ref 0)];;

(* Coin Haut Droit *)
let terrainHautDroit = [(ref 0, ref 15)];;

(* Coin Bas Gauche *)
let terrainBasGauche = [(ref 15, ref 0)];;
  
(* Coin Bas Gauche *)
let terrainBasDroit = [(ref 15, ref 15)];;

(* Bord Haut *)
let terrainHaut = [(ref 0, ref 1);
		   (ref 0, ref 2);
		   (ref 0, ref 3);
		   (ref 0, ref 4);
		   (ref 0, ref 5);
		   (ref 0, ref 6);
		   (ref 0, ref 7);
		   (ref 0, ref 8);
		   (ref 0, ref 9);
		   (ref 0, ref 10);
		   (ref 0, ref 11);
		   (ref 0, ref 12);
		   (ref 0, ref 13);
		   (ref 0, ref 14)]
;;

(* Bord Droite *)
let terrainDroite = [(ref 1, ref 0);
		     (ref 2, ref 0);
		     (ref 3, ref 0);
		     (ref 4, ref 0);
		     (ref 5, ref 0);
		     (ref 6, ref 0);
		     (ref 7, ref 0);
		     (ref 8, ref 0);
		     (ref 9, ref 0);
		     (ref 10, ref 0);
		     (ref 11, ref 0);
		     (ref 12, ref 0);
		     (ref 13, ref 0);
		     (ref 14, ref 0)]
;;

(* Bord Bas *)
let terrainBas = [(ref 15, ref 1);
		  (ref 15, ref 2);
		  (ref 15, ref 3);
		  (ref 15, ref 4);
		  (ref 15, ref 5);
		  (ref 15, ref 6);
		  (ref 15, ref 7);
		  (ref 15, ref 8);
		  (ref 15, ref 9);
		  (ref 15, ref 10);
		  (ref 15, ref 11);
		  (ref 15, ref 12);
		  (ref 15, ref 13);
		  (ref 15, ref 14)]
;;

(* Bord Gauche *)
let terrainGauche = [(ref 1, ref 15);
		     (ref 2, ref 15);
		     (ref 3, ref 15);
		     (ref 4, ref 15);
		     (ref 5, ref 15);
		     (ref 6, ref 15);
		     (ref 7, ref 15);
		     (ref 8, ref 15);
		     (ref 9, ref 15);
		     (ref 10, ref 15);
		     (ref 11, ref 15);
		     (ref 12, ref 15);
		     (ref 13, ref 15);
		     (ref 14, ref 15)]
;;

(* Centre *)
let terrain = [(ref 1, ref 1);
	       (ref 1, ref 2);
	       (ref 1, ref 3);
	       (ref 1, ref 4);
	       (ref 1, ref 5);
	       (ref 1, ref 6);
	       (ref 1, ref 7);
	       (ref 1, ref 8);
	       (ref 1, ref 9);
	       (ref 1, ref 10);
	       (ref 1, ref 11);
	       (ref 1, ref 12);
	       (ref 1, ref 13);
	       (ref 1, ref 14);
	       (ref 2, ref 1);
	       (ref 2, ref 2);
	       (ref 2, ref 3);
	       (ref 2, ref 4);
	       (ref 2, ref 5);
	       (ref 2, ref 6);
	       (ref 2, ref 7);
	       (ref 2, ref 8);
	       (ref 2, ref 9);
	       (ref 2, ref 10);
	       (ref 2, ref 11);
	       (ref 2, ref 12);
	       (ref 2, ref 13);
	       (ref 2, ref 14);
	       (ref 3, ref 1);
	       (ref 3, ref 2);
	       (ref 3, ref 3);
	       (ref 3, ref 4);
	       (ref 3, ref 5);
	       (ref 3, ref 6);
	       (ref 3, ref 7);
	       (ref 3, ref 8);
	       (ref 3, ref 9);
	       (ref 3, ref 10);
	       (ref 3, ref 11);
	       (ref 3, ref 12);
	       (ref 3, ref 13);
	       (ref 3, ref 14);
	       (ref 4, ref 1);
	       (ref 4, ref 2);
	       (ref 4, ref 3);
	       (ref 4, ref 4);
	       (ref 4, ref 5);
	       (ref 4, ref 6);
	       (ref 4, ref 7);
	       (ref 4, ref 8);
	       (ref 4, ref 9);
	       (ref 4, ref 10);
	       (ref 4, ref 11);
	       (ref 4, ref 12);
	       (ref 4, ref 13);
	       (ref 4, ref 14);
	       (ref 5, ref 1);
	       (ref 5, ref 2);
	       (ref 5, ref 3);
	       (ref 5, ref 4);
	       (ref 5, ref 5);
	       (ref 5, ref 6);
	       (ref 5, ref 7);
	       (ref 5, ref 8);
	       (ref 5, ref 9);
	       (ref 5, ref 10);
	       (ref 5, ref 11);
	       (ref 5, ref 12);
	       (ref 5, ref 13);
	       (ref 5, ref 14);
	       (ref 6, ref 1);
	       (ref 6, ref 2);
	       (ref 6, ref 3);
	       (ref 6, ref 4);
	       (ref 6, ref 5);
	       (ref 6, ref 6);
	       (ref 6, ref 7);
	       (ref 6, ref 8);
	       (ref 6, ref 9);
	       (ref 6, ref 10);
	       (ref 6, ref 11);
	       (ref 6, ref 12);
	       (ref 6, ref 13);
	       (ref 6, ref 14);
	       (ref 7, ref 1);
	       (ref 7, ref 2);
	       (ref 7, ref 3);
	       (ref 7, ref 4);
	       (ref 7, ref 5);
	       (ref 7, ref 6);
	       (ref 7, ref 7);
	       (ref 7, ref 8);
	       (ref 7, ref 9);
	       (ref 7, ref 10);
	       (ref 7, ref 11);
	       (ref 7, ref 12);
	       (ref 7, ref 13);
	       (ref 7, ref 14);
	       (ref 8, ref 1);
	       (ref 8, ref 2);
	       (ref 8, ref 3);
	       (ref 8, ref 4);
	       (ref 8, ref 5);
	       (ref 8, ref 6);
	       (ref 8, ref 7);
	       (ref 8, ref 8);
	       (ref 8, ref 9);
	       (ref 8, ref 10);
	       (ref 8, ref 11);
	       (ref 8, ref 12);
	       (ref 8, ref 13);
	       (ref 8, ref 14);
	       (ref 9, ref 1);
	       (ref 9, ref 2);
	       (ref 9, ref 3);
	       (ref 9, ref 4);
	       (ref 9, ref 5);
	       (ref 9, ref 6);
	       (ref 9, ref 7);
	       (ref 9, ref 8);
	       (ref 9, ref 9);
	       (ref 9, ref 10);
	       (ref 9, ref 11);
	       (ref 9, ref 12);
	       (ref 9, ref 13);
	       (ref 9, ref 14);
	       (ref 10, ref 1);
	       (ref 10, ref 2);
	       (ref 10, ref 3);
	       (ref 10, ref 4);
	       (ref 10, ref 5);
	       (ref 10, ref 6);
	       (ref 10, ref 7);
	       (ref 10, ref 8);
	       (ref 10, ref 9);
	       (ref 10, ref 10);
	       (ref 10, ref 11);
	       (ref 10, ref 12);
	       (ref 10, ref 13);
	       (ref 10, ref 14);
	       (ref 11, ref 1);
	       (ref 11, ref 2);
	       (ref 11, ref 3);
	       (ref 11, ref 4);
	       (ref 11, ref 5);
	       (ref 11, ref 6);
	       (ref 11, ref 7);
	       (ref 11, ref 8);
	       (ref 11, ref 9);
	       (ref 11, ref 10);
	       (ref 11, ref 11);
	       (ref 11, ref 12);
	       (ref 11, ref 13);
	       (ref 11, ref 14);
	       (ref 12, ref 1);
	       (ref 12, ref 2);
	       (ref 12, ref 3);
	       (ref 12, ref 4);
	       (ref 12, ref 5);
	       (ref 12, ref 6);
	       (ref 12, ref 7);
	       (ref 12, ref 8);
	       (ref 12, ref 9);
	       (ref 12, ref 10);
	       (ref 12, ref 11);
	       (ref 12, ref 12);
	       (ref 12, ref 13);
	       (ref 12, ref 14);
	       (ref 13, ref 1);
	       (ref 13, ref 2);
	       (ref 13, ref 3);
	       (ref 13, ref 4);
	       (ref 13, ref 5);
	       (ref 13, ref 6);
	       (ref 13, ref 7);
	       (ref 13, ref 8);
	       (ref 13, ref 9);
	       (ref 13, ref 10);
	       (ref 13, ref 11);
	       (ref 13, ref 12);
	       (ref 13, ref 13);
	       (ref 13, ref 14);
	       (ref 14, ref 1);
	       (ref 14, ref 2);
	       (ref 14, ref 3);
	       (ref 14, ref 4);
	       (ref 14, ref 5);
	       (ref 14, ref 6);
	       (ref 14, ref 7);
	       (ref 14, ref 8);
	       (ref 14, ref 9);
	       (ref 14, ref 10);
	       (ref 14, ref 11);
	       (ref 14, ref 12);
	       (ref 14, ref 13);
	       (ref 14, ref 14)]
;;

(** Pour limiter le terrain de jeu **)				
let limites =  [(ref (-1), ref 0);
		(ref (-1), ref 1);
		(ref (-1), ref 2);
		(ref (-1), ref 3);				
		(ref (-1), ref 4);
		(ref (-1), ref 5);
		(ref (-1), ref 6);				
		(ref (-1), ref 7);
		(ref (-1), ref 8);				
		(ref (-1), ref 9);				
		(ref (-1), ref 10);
		(ref (-1), ref 11);				
		(ref (-1), ref 12);
		(ref (-1), ref 13);
		(ref (-1), ref 14);
		(ref (-1), ref 15);
		
		(ref 16, ref 0);
		(ref 16, ref 1);
		(ref 16, ref 2);
		(ref 16, ref 3);				
		(ref 16, ref 4);
		(ref 16, ref 5);
		(ref 16, ref 6);				
		(ref 16, ref 7);
		(ref 16, ref 8);				
		(ref 16, ref 9);				
		(ref 16, ref 10);
		(ref 16, ref 11);				
		(ref 16, ref 12);
		(ref 16, ref 13);
		(ref 16, ref 14);
		(ref 16, ref 15);

		(ref 0, ref (-1));
		(ref 1, ref (-1));
		(ref 2, ref (-1));
		(ref 3, ref (-1));				
		(ref 4, ref (-1));
		(ref 5, ref (-1));
		(ref 6, ref (-1));				
		(ref 7, ref (-1));
		(ref 8, ref (-1));				
		(ref 9, ref (-1));				
		(ref 10, ref (-1));
		(ref 11, ref (-1));				
		(ref 12, ref (-1));
		(ref 13, ref (-1));
		(ref 14, ref (-1));
		(ref 15, ref (-1));

		(ref 0, ref 16);
		(ref 1, ref 16);
		(ref 2, ref 16);
		(ref 3, ref 16);				
		(ref 4, ref 16);
		(ref 5, ref 16);
		(ref 6, ref 16);				
		(ref 7, ref 16);
		(ref 8, ref 16);				
		(ref 9, ref 16);				
		(ref 10, ref 16);
		(ref 11, ref 16);				
		(ref 12, ref 16);
		(ref 13, ref 16);
		(ref 14, ref 16);
		(ref 15, ref 16)]
;;
  
(*** Affichages sur la grille ***)		
(** Affichage du d�cor **)
let afficher_decor () = 
  Graphics.synchronize();;

let afficher_terrain() =
  Graphics.remember_mode true;			

  List.iter (afficher Decors.terrainHautGauche) terrainHautGauche;
  List.iter (afficher Decors.terrainHautDroit) terrainHautDroit;
  List.iter (afficher Decors.terrainBasGauche) terrainBasGauche;
  List.iter (afficher Decors.terrainBasDroit) terrainBasDroit;

  List.iter (afficher Decors.terrainHaut) terrainHaut;
  List.iter (afficher Decors.terrainDroite) terrainDroite;
  List.iter (afficher Decors.terrainBas) terrainBas;
  List.iter (afficher Decors.terrainGauche) terrainGauche;
  
  List.iter (afficher Decors.terrain) terrain;
  
  List.iter (afficher Objets.rocher) rochers;
  
  Graphics.remember_mode false
;;
  
(** Affichage du personnage **)			
let afficher_perso() = 
  afficher perso (perso_i,perso_j);;

(** Affichage des �toiles **)
let afficher_etoiles() =
  List.iter (afficher Objets.etoile) etoiles;;

(** Affichage des coeurs **)
let afficher_coeurs() =
  List.iter (afficher Objets.coeur) coeurs;;
  
(** Afficharge des monstres **)
let afficher_monstres() =
  List.iter (afficher Personnage.chauveSouris) monstres;;


(** Mis en commentaire car inutile pour le moment **)
(*(** Affichage chauveSouris **)			
let afficher_chauveSouris() = 
  afficher chauveSouris (chauveSouris_i, chauveSouris_j);;
  
(** Affichage serpent **)			
let afficher_serpent() = 
  afficher serpent (serpent_i, serpent_j);;

(** Affichage grenouille **)			
let afficher_grenouille() = 
  afficher grenouille (grenouille_i, grenouille_j);;

(** Affichage araignee **)			
let afficher_araignee() = 
  afficher araignee (araignee_i, araignee_j);;
  
(** Affichage fantome**)			
let afficher_fantome() = 
  afficher fantome (fantome_i, fantome_j);;

(** Affichage abeille **)			
let afficher_abeille() = 
  afficher abeille (abeille_i, abeille_j);;
 *)



(** Affichage des objets, monstres et personnage **)
(* qui peuvent changer de place sur la grille *)			
let afficher_mobiles () = 
  afficher_coeurs();
  afficher_etoiles();
  afficher_monstres();
  afficher_perso ()
;;

  
(*** D�placements par direction ***)
let deplacer direction =
  let () = match direction with
    | E -> perso_j := !perso_j + 1
    | O -> perso_j := !perso_j - 1
    | S -> perso_i := !perso_i + 1
    | N -> perso_i := !perso_i - 1
  in	
  
  (** Pendant qu'on se d�place **)
  (* Les rochers sont une muraille de roche *)
  let muraille_de_roche (mi, mj) = 
    if(!mi,!mj) = (!perso_i,!perso_j) then (match direction with
					    | E -> begin perso_j := (!perso_j - 1); print_string "Zut...C'est bloqu�! \n" end
					    | O -> begin perso_j := (!perso_j + 1); print_string "Zut...C'est bloqu�! \n" end
					    | S -> begin perso_i := (!perso_i - 1); print_string "Zut...C'est bloqu�! \n" end
					    | N -> begin perso_i := (!perso_i + 1); print_string "Zut...C'est bloqu�! \n" end)
  in	
  
  (** Pendant qu'on se d�place **)
  (* Les monstres ne peuvent pas �tre transperc� et si on se d�place sur un monstre, un point de vie est retir� *)
  let collision_monstres (mi, mj) = 
    if(!mi,!mj) = (!perso_i,!perso_j) then (match direction with
					    | E -> begin perso_j := (!perso_j - 1); 
							 print_string "A�e!\n"; 
							 nb_pointsDeVie_perso := !nb_pointsDeVie_perso - 1 end
					    | O -> begin perso_j := (!perso_j + 1); 
							 print_string "A�e!\n"; 
							 nb_pointsDeVie_perso := !nb_pointsDeVie_perso - 1 end
					    | S -> begin perso_i := (!perso_i - 1); 
							 print_string "A�e!\n"; 
							 nb_pointsDeVie_perso := !nb_pointsDeVie_perso - 1 end
					    | N -> begin perso_i := (!perso_i + 1); 
							 print_string "A�e!\n"; 
							 nb_pointsDeVie_perso := !nb_pointsDeVie_perso - 1 end)
  in

  (** Pendant qu'on se d�place **)
  (* Il est interdit de quitter la grille de jeu *)
  let interdit_de_sortie (ii, ij) =  
    if(!ii,!ij) = (!perso_i,!perso_j) then (match direction with
					    | E -> begin (if(!ij = -1) then perso_j := 0); print_string "Interdit de sortie ! \n" end
					    | O -> begin (if(!ij = 16) then perso_j := 15); print_string "Interdit de sortie ! \n" end
					    | S -> begin (if(!ii = -1) then perso_i := 0); print_string "Interdit de sortie ! \n" end
					    | N -> begin (if(!ii = 16) then perso_i := 15); print_string "Interdit de sortie ! \n" end)		
  in
  
  (*
	let verifier_sortie_trouvee (si, sj) =  
		if(!si,!sj) = (!perso_i,!perso_j) then
			if(!si,!sj) = (14,14) then
			begin
				sortie_trouvee := !sortie_trouvee + 1 ;
				print_string "Vous avez trouv� la sortie ! \n";
			end
	in	
   *)
  
  let ramasser_etoile (ri,rj) =
    if (!ri,!rj) = (!perso_i,!perso_j) then
      begin
	ri := -1;
	rj := -1;
      end
  in

  let ramasser_coeur (ci,cj) =
    if (!ci,!cj) = (!perso_i,!perso_j) then
      begin
	ci := -1;
	cj := -1;
	if (!nb_pointsDeVie_perso < 3) then
	  begin
	    nb_pointsDeVie_perso := !nb_pointsDeVie_perso + 1 ;
	    print_string "Vous avez r�cup�r� un coeur ! \n";
	  end 
	else
	  begin
	    print_string "Vous avez d�j� 3 coeurs !\n";
	  end
      end
  in
  
  let etoiles_ramassees () =
    let rec verification_etoiles_ramassees e = match e with
      | [] -> print_string "Bravo ! Vous avez r�cup�r� toutes les �toiles! \n"
      | [(x,y)] -> begin if (!x,!y) = (-1,-1) then print_string "Bravo ! Vous avez r�cup�r� toutes les �toiles! \n" end
      | (x,y)::xs -> if (!x,!y) <> (-1,-1) then verification_etoiles_ramassees xs else ()
    in verification_etoiles_ramassees etoiles
  in  

  let coeurs_restants () = 
    print_string "Vous avez encore "; print_int(!nb_pointsDeVie_perso); print_string " coeur(s).\n"
  in
  
  let perdu_chance() =
    if nb_pointsDeVie_perso = ref 0 and nb_chance = ref 0 then
      begin
	print_string "Quel dommage ! Vous n'avez plus de vie ! Mais comme nous sommes gentil, on vous laisse une autre chance !\n Bonne chance !\n";
	perso_i := 8;
	perso_j := 8;
	nb_pointsDeVie_perso := !nb_pointsDeVie_perso + 3 ;
	nb_chance := !nb_chance + 1 ;
      end
  in
  
  let perdu() =
    if nb_pointsDeVie_perso = ref 0 and nb_chance = ref 1 then
      begin
	print_string "Vous avez rat� votre chance de gagner, la partie est finie. Vous ferez surement mieux la prochaine fois ! \n";
      end
  in
  
  (***** TODO *****)
  (* Partie Perdue si coeurs < 1 *)
  (* Partie Gagn�e si etoiles_ramassees OK et sortie_trouvee OK *)
  (* Utiliset Graphics pour afficher un message sur l'�cran "Gagn�" ou "Perdu" *)

  List.iter muraille_de_roche rochers;
  List.iter interdit_de_sortie limites;
  List.iter collision_monstres monstres;
  perdu_chance();
  perdu();
  List.iter ramasser_etoile etoiles;
  List.iter ramasser_coeur coeurs;
  (*List.iter attaquer_monstre monstres; *)
  (* verifier_sortie_trouvee (); *)
  afficher_decor ();
  afficher_mobiles (); (* Etoiles, Rochers, Coeurs, Monstres Vivants et Perso *)
  etoiles_ramassees ();
  coeurs_restants ()
;;
  
(*let attaquer direction (ai,aj) =
  if (!ai,!aj) = (!perso_i,!perso_j) then
    begin
      if ((!Random.int 100) < 50) then nb_pointsDeVie_perso := !nb_pointsDeVie_perso - 1 ; print_string "Vous avez perdu un coeur ! \n"		 
      else ai := -1 ; aj := -1 ; print_string "Vous avez tu� un monstre ! \n"		
    end*)
let attaquer direction =
  let attaquer_bas (i,j) =
    if (!i,!j) = ((!perso_i) + 1,!perso_j) then
      begin
	i := -1;
	j := -1;
      end
  in
  let attaquer_haut (i,j) =
    if (!i,!j) = ((!perso_i) - 1,!perso_j) then
      begin
	i := -1;
	j := -1;
      end
  in
  let attaquer_gauche (i,j) =
    if (!i,!j) = (!perso_i,(!perso_j) - 1) then
      begin
	i := -1;
	j := -1;
      end
  in
  let attaquer_droite (i,j) =
    if (!i,!j) = (!perso_i,(!perso_j) + 1) then
      begin
	i := -1;
	j := -1;
      end
  in
  if (direction = N) then
    begin
      List.iter attaquer_haut monstres;
    end;
  if (direction = S) then
    begin
      List.iter attaquer_bas monstres;
    end;
  if (direction = E) then
    begin
      List.iter attaquer_droite monstres;
    end;
  if (direction = O) then
    begin
      List.iter attaquer_gauche monstres;
    end;
  afficher_decor ();
  afficher_mobiles ();
;;


  
(*** Fonction jouer ***)
(** Utilise la fonction run de l'interpr�te **)
(* Dans "interprete.ml" *)
let jouer f p = Interprete.run f p;;
  
print_string "\n*****************************************************************************\n";;
print_string "------------------------------------------------------------------------------\n";;
print_string "************************                               ***********************\n";;
print_string "------------------------  		  LABYMON		     -----------------------\n";;
print_string "************************                               ***********************\n";;
print_string "------------------------------------------------------------------------------\n";;
print_string "******************************************************************************\n";;
print_string "\n";;
	    
(* Cr�dits images & Jeu *)
print_string " Cr�dit image : Kenney Vleugels - http://www.kenney.nl/\n";;
print_string " Projet encadr� par : Monsieur Pierre BOUDES \n";;
print_string " Jeu par : Steven YUNG & Marie-Eve PICARD GRAVEL \n";;
print_string "\n";;
print_string "       Combat les monstres, collecte les �toiles et trouve la sortie ! \n";;


(* ----------------------------------  TODO Donner TOUTES les r�gles pour jouer avec l'interpr�teur ---------------------------- *)
print_string "\n";;
print_string "       Pour bouger d'une case dans une direction, entrez par exemple :  \n";;
print_string "       jouer Nord | jouer Sud | jouer Est | jouer Ouest   \n";;

print_string "       Pour bouger de plusieurs cases entrez par exemple :  \n";;
print_string "       jouer (Bloc[Nord;Nord;Est;Est;Est])  \n";;
print_string "\n";;

(*** Mise en place du terrain ***)
afficher_terrain();;
						  
afficher_mobiles ();;
					  
