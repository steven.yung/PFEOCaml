open Directions;;

(*** On cr�e un type expression  ***)
(** Pour permettre l'�valuation d'expressions dans les instructions **)
type expression = 
  | Entier of int
  | Plus of expression * expression
  | Mult of expression * expression
  | Div of expression * expression
  | Moins of expression * expression 
  | Opp of expression 
  | Egal of expression * expression
  | NonEgal of expression * expression
  | Superieur of expression * expression 
  | Inferieur of expression * expression
;;

(*** On cr�e l'�valuation de mani�re r�cursive  ***)
(** Pour permettre l'�valuation des expressions  **)			
let rec evaluer exp = 
  match exp with
  | Entier n -> n
  | Plus (exp1, exp2) -> ((evaluer exp1) + (evaluer exp2))
  | Mult (exp1, exp2) -> ((evaluer exp1) * (evaluer exp2))
  | Div (exp1, exp2) -> ((evaluer exp1) / (evaluer exp2))
  | Moins (exp1, exp2) -> ((evaluer exp1) - (evaluer exp2))
  | Opp (exp) -> ((-1) * (evaluer exp))
  | Egal (exp1, exp2) -> (match (evaluer exp1) = (evaluer exp2) with 
			  | true -> 1
			  | false -> 0)
  | NonEgal (exp1, exp2) -> (match (evaluer exp1) != (evaluer exp2) with 
			     | true -> 1
			     | false -> 0)
  | Superieur (exp1, exp2) -> (match (evaluer exp1) > (evaluer exp2) with 
			       | true -> 1
			       | false -> 0)			
  | Inferieur (exp1, exp2) -> (match (evaluer exp1) < (evaluer exp2) with 
			       | true -> 1
			       | false -> 0)
;;
  
(*** On cr�e le type instruction  ***)			
type instruction = 
			| Est 
			| Ouest 
			| Nord 
			| Sud 
			| Bloc of instruction list
			(*
			| PrintStr of string
			| PrintE of expression
			| While of expression * instruction
			*)
			;;


(*** La fonction run permet l'interpr�tation des instructions ***)				
let rec run f p =
  match p with
    | Est -> f E
    | Ouest -> f O
    | Sud -> f S
    | Nord -> f N
    | Bloc (x::xs) -> run f x; run f (Bloc xs)
    | Bloc [] -> ()
	(*
	| PrintStr (chaineDonnee) -> print_string (chaineDonnee)
	| PrintE (exp) -> print_int (evaluer exp)
	| While (expression, instruction) as myWhile -> (match (evaluer expression) with 
													| 0 -> ()
													| _ -> run instruction f; run myWhile f)
	*)
	;;


